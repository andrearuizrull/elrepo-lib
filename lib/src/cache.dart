/*
 *  elRepo.io decentralized culture repository
 *
 *  Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */

import 'package:elrepo_lib/models.dart';
import 'package:retroshare_dart_wrapper/rs_models.dart';
import 'package:elrepo_lib/repo.dart' as repo;
import 'package:retroshare_dart_wrapper/retroshare.dart' as rs;

/// This file will contain caches for needing information
///
/// May we check [this](https://github.com/ivoleitao/stash) for more advanced
/// cache implementation.

class Caches {
  static final userBookmarksCache = UserBookmarksCache();
  static final identitiesCache = IdentitesCache();
  static final circlesCache = CirclesCache();
}

/// This cache stores message metadata for bookmarks
///
/// The bookmark cache only need to be updated when a post is added to the cache
/// because is the only interaction with the BOOKMARK forums, that will be
/// always referring posts.
class UserBookmarksCache{
  var _userBookmarksCache = <RsMsgMetaData>[];

  UserBookmarksCache();

  /// Get [_userBookmarksCache] or [update] it if empty
  Future<List<RsMsgMetaData>> get data async => _userBookmarksCache.isNotEmpty
      ? _userBookmarksCache : await update();

  Future<List<RsMsgMetaData>> update() async =>
      _userBookmarksCache = await repo.getPostHeaders('BOOKMARKS', rs.authIdentityId);

  /// Return a list of the referred posts ids
  ///
  /// Used on [repo.exploreContent] to delete from explore bookmarked posts. It
  /// take original referredPostId from the bookmark post metadata.
  Future<List<String>> get referredPostIds async =>
    (await data).map((e) => e.postMetadata.referred).cast<String>().toList();

  /// Used to retrieve the data from non async functions.
  ///
  /// Could return an empty list in case  _userBookmarksCache is not updated yet
  List<RsMsgMetaData> get dataSync => _userBookmarksCache;

}

class IdentitesCache{
  // todo: cache identity details also

  /// Store own identity info
  Identity _ownIdentity;
  Future<Identity> get ownIdentity async =>
      _ownIdentity ?? await updateOwnIdentity();
  /// Update cached [_ownIdentity] from RS and return it.
  Future<Identity> updateOwnIdentity () async =>
      _ownIdentity = await repo.getIdDetails(rs.authIdentityId);

  /// Map that store [Identity.mId] and [Identity]
  var _rsGxsToIdentityMap = <String, Identity>{};
  Map<String, Identity> get rsGxsToIdentityMap => _rsGxsToIdentityMap;

  /// From [idList] update [_rsGxsToIdentityMap]
  Future<void> _updateRsGxsToIdentityMap(List<Identity> idList) async{
    print('Updating getIdentitiesSummaries cache');
    _rsGxsToIdentityMap = {for (var e in idList) e.mId: e};
  }

  /// Call [rs.RsIdentity.getIdentitiesSummaries] and asynchronous call
  /// [_updateRsGxsToIdentityMap] with the result
  ///
  /// /// It return a fresh [getIdentitiesSummaries] result but update the cache.
  Future<List<Identity>> updateSummaries() async {
    var idSummaries = await rs.RsIdentity.getIdentitiesSummaries();
    _updateRsGxsToIdentityMap(idSummaries);
    return idSummaries;
  }
}

class CirclesCache{

  /// Map with relation between CircleId with CircleJson object
  /// todo: Implement circle as model
  var _subscribedCirclesMap = <String, Map<String, dynamic>>{};

  /// Synchronous function that can return empty cached circles
  ///
  /// It start caching asynchronous if empty cache
  Map<String, Map<String, dynamic>> get subscribedCirclesCached {
    if(_subscribedCirclesMap.isEmpty) getCirclesSummaries();
    return _subscribedCirclesMap;
  }

  /// If no cached circles call getCirclesSummaries.
  ///
  /// This cache implementation in asyncronous.
  Future<Map<String, Map<String, dynamic>>> get subscribedCircles async {
    if(_subscribedCirclesMap.isEmpty) {
      await _updateCache(await rs.RsGxsCircles.getCirclesSummaries());
    }
    return _subscribedCirclesMap;
  }

  /// From a fresh circle summaries list it create the cached lists
  Future<void> _updateCache (List<Map<String, dynamic>> circleList) async {
    _subscribedCirclesMap = {
      for (var e in circleList..removeWhere((circle) => circle['mSubscribeFlags'] != 7))
        e['mGroupId']: e
    };
  }

  /// Return fresh getCirclesSummaries list and update [_subscribedCirclesMap]
  /// cache asynchronous
  Future<List<Map<String, dynamic>>> getCirclesSummaries() async {
    var circleList = await rs.RsGxsCircles.getCirclesSummaries();
    _updateCache(circleList);
    return circleList;
  }
}
