/*
 *  elRepo.io decentralized culture repository
 *
 *  Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */

part of '../repo.dart';

/// Return a future list of sslids details.
///
/// The details object is modified and added a boolean isConnected value
Future<List<dynamic>> getSslPeerList() async {
  var onlineList = await rs.RsPeers.getOnlineList();
  var result = [];
  if (onlineList.isNotEmpty) {
    result.addAll([
      for (var sslId in onlineList)
        await rs.RsPeers.getPeerDetails(sslId)
          ..['isConnected'] = true
    ]);
  }
  var offlineList = await rs.RsPeers.getFriendList();
  if (offlineList.isNotEmpty) {
    offlineList.removeWhere((sslId) => onlineList.contains(sslId));
    result.addAll([
      for (var sslId in offlineList)
        await rs.RsPeers.getPeerDetails(sslId)
          ..['isConnected'] = false
    ]);
  }
  return result;
}


/// Execute rs.RsBroadcastDiscovery.getDiscoveredPeers() and add all returned
/// peers as friends.
Future<List> doPromiscuity() async {
  final peers = await rs.RsBroadcastDiscovery.getDiscoveredPeers();
  var toAdd = [];
  if (peers.isNotEmpty) {
    for (var peer in peers) {
      if (!await rs.RsPeers.isFriend(peer['mSslId'])) {
        toAdd.add(peer);
      }
    }
    print('Peers found: $peers');
    rs.addFriends(toAdd);
  }
  return toAdd;
}


/// Separate funcion to add the list of tiers.
///
/// Return true if one befriendings are success.
Future<bool> befriendingTiers() async {
  var margaretsanger = await befriendTier1(hostname: 'margaretsanger');
  var robertocarlos = await befriendTier1(hostname: 'robertocarlos');
  return margaretsanger || robertocarlos;
}

/// Execute befriending with a Tier1
///
/// It try to befriend with a [hostname] on the [baseUrl]
/// `http://$hostname.$baseUrl/GetRetroshareInvite` and
/// `http://$hostname.$baseUrl/acceptInvite`
///
/// Return true if success false otherwhise printing an error.
Future<bool> befriendTier1(
    {String hostname = 'michelangiolillo',
      String baseUrl = 'elrepo.io/rsPeers'}) async {
  print('Befriending $hostname');

  // Get RS invite process
  var reqUrl = 'http://$hostname.$baseUrl/GetRetroshareInvite';
  try {
    final response = await http.get(Uri.parse(reqUrl));
    Map decoded;
    if (response.statusCode == 200) {
      decoded = jsonDecode(response.body);
    } else {
      print(
          'Error receiving Tier1 invite. Status code: ${response.statusCode}');
      return false;
    }
    String tier1Cert = decoded['retval'];
    rs.RsPeers.acceptInvite(tier1Cert);
  } catch (error) {
    print('Cannot reach $hostname. Error: $error');
    return false;
  }

  // Send RS invite process
  final myInvite = await rs.RsPeers.getRetroshareInvite();
  reqUrl = 'http://$hostname.$baseUrl/acceptInvite';
  var jsonCert = {'invite': myInvite};
  try {
    final response =
    await http.post(Uri.parse(reqUrl), body: jsonEncode(jsonCert));
    if (response.statusCode != 200) {
      print('Error sending cert to $hostname. Code ${response.statusCode}');
    }
    return true;
  } catch (error) {
    print('Connection to $hostname failed. Error: $error ');
  }
  return false;
}

/// Check if a [pgpId] is a tier one friend
bool isTierOne(String pgpId) => pgpId == TIER1_PGP_ID;

/// Check if is connected to a retroshare peer
///
/// It return [RsNetworkStatus], check it for documentation of the status.
Future<RsNetworkStatus> checkRsNetworkStatus() async{
  var sslIds = await rs.RsPeers.getOnlineList();
  if (sslIds.isEmpty) return RsNetworkStatus.NO_PEERS;
  for(var sslId in sslIds){
    if(isTierOne(await rs.RsPeers.getGPGId(sslId))) return RsNetworkStatus.TIER_CONNECTED;
  }
  return RsNetworkStatus.PEER_CONNECTED;
}

/// Status of the retroshare network.
///
/// Three status:
/// [NO_PEERS]: No peers connected
/// [TIER_CONNECTED]: Tier one peer connected. Check if is tier one using
/// hardcoded pgpId.
/// [PEER_CONNECTED]: a peer is connected
enum RsNetworkStatus {NO_PEERS, TIER_CONNECTED, PEER_CONNECTED}
