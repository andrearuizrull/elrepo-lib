/*
 *  elRepo.io decentralized culture repository
 *
 *  Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */

part of '../repo.dart';

Future<String> getAccountStatus() async {
  final isLoggedIn = await rs.RsLoginHelper.isLoggedIn();
  if (isLoggedIn) {
    print('Is logged in');
    return cnst.AccountStatus.isLoggedIn;
  }
  else if (await rs.RsLoginHelper.hasLocation()) {
    print('Has Location -> Login');
    return cnst.AccountStatus.hasLocation;
  } else {
    print('Does Not Have Location -> SignUp');
    return cnst.AccountStatus.hasNoLocation;
  }
}

Future<int> login(String password, [Map location]) async {
  if (location == null) {
    location = await rs.RsLoginHelper.getDefaultLocation();
    print('logging in $location');
  }
  final responseStatus = await rs.RsLoginHelper.login(location, password);
  return responseStatus;
}

Future<Map> signUp(String password, String locationName,
    {String api_user}) async {
  print('creating account for $locationName');
  final location = await rs.RsLoginHelper.createLocation(locationName, password,
      api_user: api_user);
  print('Location: $location');
  if (api_user != null) {
    // Set the API user to start to work with. This is used on the gateway where
    // the default api user is not elrepo.io
    rs.authApiUser = api_user;
  }
  return location;
}

/// Do a first publication in order to propagate the identity.
///
/// To start propagate an identity to find it on circles screen is needec to
/// publish at least on time in one forum.
///
/// First publication will be done in a bookmarks forum with role
/// "first_publication". Where the referreds we'll store identity id and
/// nickname. Probably we are not going to use it never, but who knows on the
/// future...
Future<bool> firstPublication () async =>
    await bookmarkPost(
        rs.authIdentityId,
        rs.authLocationName,
        PostMetadata(title: 'Hello World!',
            summary: 'First publication',
            markup: MarkupTypes.plain,
            contentType: ContentTypes.ref,
            role: PostRoles.firstPublication)
    );

/// Function that initialize elrepo.io for each startup
///
/// It execute a befriending with Tiers to excahange the RS invites. It also
/// execute [subscribeToRepoForums] and [rs.RsGxsForum.requestSynchronization].
///
/// Finally also cache some stuff like circles, bookmarks, identities...
Future<bool> prepareSystem() async {
  // Is a good idea to exchange RS invites each time to update the public IP of the nodes
  befriendingTiers();

  // It subscribe to all `elRepo_${cnst.API_VERSION}_.` forums and request sync
  await subscribeToRepoForums();
  rs.RsGxsForum.requestSynchronization();

  // Cache circle details on RetroShare to access to their information faster
  // getCirclesDetails(); // This will be done on the Ui using providers

  // Cache on memory the bookmarked posts
  Caches.userBookmarksCache.update();

  // Cache identity summaries
  Caches.identitiesCache.updateSummaries();

  // Follow tags
  // todo: should not be needed if we do subscribeToRepoForums before
  getTagNames();

  return true;
}

/// On RS set the absolute path where download and partial directory have to be.
Future<bool> setDownloadAndPartialDirectory(downloadDir, partialsDir) async {
  print(
      'Setting Retroshare directories. \n Download and shared: $downloadDir\n Partials: $partialsDir');
  rs.RsFiles.setDownloadDirectory(downloadDir);
  addSharedRepoDirectory(downloadDir);
  rs.RsFiles.setPartialsDirectory(partialsDir);
  return true;
}

/// Add shared folder
///
/// return true if is already shared or if [rs.RsFiles.addSharedDirectory] is
/// successful. Return false in case [rs.RsFiles.addSharedDirectory] retval is
/// false.
Future<bool> addSharedRepoDirectory(String path) async {
  var alreadyShared = await rs.isDirectoryAlreadyShared(path);
  return alreadyShared
      ? alreadyShared
      : await rs.RsFiles.addSharedDirectory(path,
      virtualname: 'elRepo.io', shareflags: 0x80);
}


