/*
 *  elRepo.io decentralized culture repository
 *
 *  Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */

part of '../repo.dart';

Future<String> findOrCreateRepoForum(
    {String forumType = 'CONTENT',
      String label = '',
      String circleId = ''}) async {
  if (!forumTypes.contains(forumType)) {
    throw Exception('invalid forum type');
  }
  if (label != '') {
    label = '_$label';
  }
  var forumName = "${cnst.FORUM_PREPEND}${cnst.API_VERSION}_$forumType$label";
  print('Find or create $forumName');
  var result = await rs.RsGxsForum.getForumsSummaries();
  var elRepoForums = result
      .where((i) => ((i['mGroupName'] == forumName &&
      circleId.isEmpty &&
      i['mCircleId'] == '00000000000000000000000000000000') ||
      (i['mGroupName'] == forumName &&
          (circleId.isNotEmpty && i['mCircleId'] == circleId))))
      .toList();
  var forumId;
  if (elRepoForums.isNotEmpty) {
    print('Found existing elRepo.io forums');
    forumId = elRepoForums[0]['mGroupId'];
  } else {
    forumId = await rs.RsGxsForum.createForumV2(forumName, circleId: circleId);
    print('No elRepo.io forums found, creating: $forumName');
    subscribeToForum(forumId, true);
  }
  return forumId;
}

/// Publish post to RetroShare node
///
/// It search for #tags inside [models.PostData.mBody.text] if [parseTags] is not
/// defined.
///
/// It publish a file, getting it hash, maybe listening an event. So until the
/// file hash is not calculated, the process is paused, because the hash is
/// need for the post metadata.
///
/// Also creates tag, content, and user forums.
///
/// [cb] function is a callback function to get feedback about the status of the
/// publication, see [PublishEvents]. The function must implement to variables,
/// the event itself defined by [PublishEvents] and a [String], that depends on
/// the type of event is diferent or empty. See the code below to see when and
/// what passes.
Future<models.PublishData> publishPost(models.PostData postData,
    {bool parseTags = true, Function(PublishEvents, String) cb}) async {
  var callback =
      cb != null; // Used as shortcut to know if call the callback or not

  // Check if a file path is defined to add the file to RS
  if (postData.filePath != null && postData.filePath != '') {
    if (callback) cb(PublishEvents.HASHING_FILE, postData.filename);
    var fileHash = '';
    try {
      // The file could be already shared (downloaded or published in another post)
      if (await rs.isDirectoryAlreadyShared(postData.filePath)) {
        print('The file was already shared: ${postData.filePath}');
        // If dirInfo here is null could mean that the file is not already hashed
        var dirInfo = await rs.findAFileOnDiretoryTree(postData.filePath);
        if (dirInfo == null) throw ('Probably the file is not hashed already');
        fileHash = dirInfo.hash;
      }
      // If is not shared we are going to wait until file hash event is fired
      else {
        print('Hashing new file: ${postData.filePath}');
        final hashingFile = await rs.RsFiles.addSharedDirectory(
            postData.filePath,
            shareflags: 2176);
        if (hashingFile) fileHash = await rs.waitForFileHash(postData.filePath);
      }
    } catch (e) {
      print('Error getting file hash for ${postData.filePath}');
      rethrow;
    }

    if (fileHash.isNotEmpty) {
      print('Hashing done for ${postData.filePath}');
      try {
        var fileInfo = await rs.RsFiles.alreadyHaveFile(fileHash);
        var fileLink = await rs.RsFiles.exportFileLink(
            fileInfo['hash'], fileInfo['size']['xint64'], postData.filename);

        postData.mBody.payloadLinks
            .add(models.PayloadLink(postData.filename, fileHash, fileLink));
        postData.metadata.contentType =
            _getContentTypeFromPath(postData.filePath);
      } catch (e) {
        print(e);
        throw ("Can't generate file link ${postData.filePath}");
      }
    } else {
      throw ('Error getting file hash for ${postData.filePath}');
    }
  }

  if (callback) cb(PublishEvents.PUBLISHING_CONTENT, '');

  // If no summary is specified, create one from body
  postData.metadata.summary ??= summaryFromBody(postData.mBody.text);

  // Find hashtags inside publication text
  if (parseTags) {
    postData.mBody.tags += findHashTags(postData.mBody.text);
    postData.mBody.tags += findHashTags(postData.metadata.title);
  }

  final forumId =
  await findOrCreateRepoForum(circleId: postData.metadata.circle ?? '');
  final msgId = await rs.RsGxsForum.createPost(
      forumId,
      jsonEncode(postData.metadata),
      jsonEncode(postData.mBody),
      rs.authIdentityId);

  final linkPostMetadata = jsonEncode(
      models.PostMetadata.generateLinkPostMetadata(
          postData.metadata, msgId, forumId));

  for (var tag in postData.mBody.tags) {
    if (callback) cb(PublishEvents.CREATING_TAGS, tag);

// Add the reference to the real post
    final tagForumId = await findOrCreateRepoForum(
        forumType: 'TAG', label: tag, circleId: postData.metadata.circle ?? '');
    print('Creating post in $tagForumId');
// Don't save the message body in a linkPost
    await rs.RsGxsForum.createPost(
        tagForumId, linkPostMetadata, '', rs.authIdentityId);
  }
  if (callback) cb(PublishEvents.BOOKMARKING, '');
// Also create a linkPost in the user forum to keep track of content published.
  final userForumId = await findOrCreateRepoForum(
      forumType: 'USER',
      label: rs.authIdentityId,
      circleId: postData.metadata.circle ?? '');
  // This await is needed because if you are testing sometimes the program finishes before create userforum post
  await rs.RsGxsForum.createPost(
      userForumId, linkPostMetadata, '', rs.authIdentityId);

  return models.PublishData(forumId, msgId, postData.mBody.tags);
}

/// Used when [publishPost] to get a summary for the given body.
///
/// Just get a number of characters from the body
String summaryFromBody(bodyText) =>
    bodyText.length > cnst.SUMMARY_LENGTH
        ? '${bodyText.substring(0, cnst.SUMMARY_LENGTH)}...' : bodyText;

/// From the filepath get the mime type
String _getContentTypeFromPath(String filePath) {
  final fileName = path.basename(filePath);
  final fileType = mime.lookupMimeType(fileName);
  var fileRootType = '';
  if (fileType is String) {
    fileRootType = fileType.split('/')[0];
  }
  return fileRootType;
}

/// Find all words with leading `#` for a given [lookupString]
List<String> findHashTags(String lookupString) {
  var exp = RegExp(r'(#(?:[^\x00-\x7F]|\w)+)');
  var matches = exp.allMatches(lookupString);
  var hasTagsList = <String>[];
  if (matches.isNotEmpty) {
    hasTagsList = matches.map((x) => x[0].substring(1)).toList();
  }
  print('Found tags: $hasTagsList');
  return hasTagsList;
}

Future<String> createComment(String forumId, String metadata,
    String commentBody, String parentPostId) async {
  final msgId = await rs.RsGxsForum.createPost(
      forumId, metadata, commentBody, rs.authIdentityId, parentPostId);
  return msgId;
}
