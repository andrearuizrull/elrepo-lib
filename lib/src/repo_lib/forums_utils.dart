/*
 *  elRepo.io decentralized culture repository
 *
 *  Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */

part of '../repo.dart';

// Contains sort and delete and other kind of functions


/// Sort a post headers list
///
/// It return at the beggining by followed authors and by timestamp, and then
/// the unfollowed authors by timestamp.
Future<List<RsMsgMetaData>> sortPostList(List<RsMsgMetaData> contentsList) async {
  var followedAuthorsIds = await getFollowedAuthorsInfo()..map((e) => e['mGroupId']);
  var followedPosts = <RsMsgMetaData>[];
  for (var post in contentsList) {
    if (followedAuthorsIds.contains(post.mAuthorId)) {
      followedPosts.add(post);
      contentsList.remove(post);
    }
  }
  return sortRsMsgMetaDataByTimestamp(contentsList)
    ..addAll(sortRsMsgMetaDataByTimestamp(followedPosts));
}

/// From a list of RsMsgMetaData return the list sorted by timestamp
///
/// It uses `b.mPublishTs.xint64.compareTo(a.mPublishTs.xint64)` as value.
List<RsMsgMetaData> sortRsMsgMetaDataByTimestamp(List<RsMsgMetaData> headers) =>
    headers..sort((a,b) => b.mPublishTs.xint64.compareTo(a.mPublishTs.xint64));

/// Delete from a list of posts headers all the ocurrences on other.
///
/// Used to search ocurrences from a list of post headers into another and
/// remove it. Where [original] is the main list, where to search the ocurrences,
/// and [newList] is the list that we return and where we are going to delete
/// the repeated ocurrences.
List<RsMsgMetaData> deleteFromPostList(
    List<RsMsgMetaData> original, List<RsMsgMetaData> newList) {
  newList.removeWhere((newElement) {
    for (var post in original) {
      if (post.mMsgId == newElement.mMsgId) return true;
    }
    return false;
  });
  return newList;
}
