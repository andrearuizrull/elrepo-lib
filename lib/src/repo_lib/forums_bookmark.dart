/*
 *  elRepo.io decentralized culture repository
 *
 *  Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */

part of '../repo.dart';

/// Add a publication to bookmarks forum
///
/// [referredForumId] is the CONTENT forum id and the [referredPostId] is the
/// post inside the CONTENT forum that will link.
///
/// On the elrepoio-android it prevents to bookmark an own post, but not here.
/// So you have to prevent to bookmark an own post before calling this function.
///
/// If bookmark added return true, if duplicated return false.
Future<bool> bookmarkPost(String referredForumId, String referredPostId,
    models.PostMetadata postMetadata) async {
  final bookmarksForumId = await findOrCreateRepoForum(
      forumType: 'BOOKMARKS', label: rs.authIdentityId);
  print('Checking for bookmark in forum: $bookmarksForumId');
  final referred = '$referredForumId $referredPostId';
  final bookmarkHeaders = await getPostHeaders('BOOKMARKS', rs.authIdentityId);
  // Check for duplicates
  for(var bookmark in bookmarkHeaders){
    if(jsonDecode(bookmark.mMsgName)['referred'] == referred) return false;
  }
  final linkPostMetadata = models.PostMetadata.generateLinkPostMetadata(
      postMetadata, referredPostId, referredForumId);
  await rs.RsGxsForum.createPost(bookmarksForumId,
      json.encode(linkPostMetadata.toJson()), '', rs.authIdentityId);
  await Caches.userBookmarksCache.update();
  return true;
}

/// @deprecated in favour of [getBookmarkPosts]
///
/// Get Bookmarked post metadata.
///
/// Get original "post metadata" of CONTENT forum from BOOKMARKS "post metadata".
///
/// If [downloading] is true return just downloading bookmarks, else return already
/// downloaded bookmarks.
Future<List<RsMsgMetaData>> getBookmarksPostMetadata({bool downloading = false}) async {
  print('getBookmarksPostMetadata downloading: $downloading');
  // Get original post headers from referred links
  var referredLinks = getReferredsFromPostHeader(await Caches.userBookmarksCache.data);

  // Get the hashes to separate these that are downloading
  var hashes = await getFileDownloads();

  // For each referred, get forum content to get the file hash data
  var returnList = <dynamic>[];
  for (var forumId in referredLinks.keys) {
    var x =
    await rs.RsGxsForum.getForumContent(forumId, referredLinks[forumId]);
    returnList.addAll(x);
  }
  // Remove the posts depending of [downloading]
  returnList.removeWhere((msg) {
    // Delete malformed if body is malformed. Can happen due to a bad referred content
    if (msg['mMsg'] == '') return true;
    var postbody = models.PostBody.fromJson(msg['mMsg']);
    // If payloadlinks are not empty mean that can be downloading content
    if (postbody.payloadLinks.isNotEmpty) {
      for (var payloadLink in postbody.payloadLinks) {
        // Here we check if the payload link is already downloaded or downloading
        // If downloading activated delete already downloaded
        // Else delete downloading
        if (downloading && hashes.contains(payloadLink.hash)) return false;
        else if (hashes.contains(payloadLink.hash)) return true;
      }
    }
    // If payload link is empty mean that is content without file, so remove it
    // if you are looking downloading content
    return downloading;
  });
  return sortPostList(returnList.map((x) => x = RsMsgMetaData.fromJson(x['mMeta'])).toList());
}

/// Get bookmarks post [RsMsgMetaData] independently if they are being download
/// or not.
///
/// Using the extension [ParsePostMetadata] we add isDownloading parameter.
Future<List<RsMsgMetaData>> getBookmarkPosts() async {
  print('getBookmarksPostMetadata downloading');
  // Get original post headers from referred links
  var referredLinks = getReferredsFromPostHeader(await Caches.userBookmarksCache.data);
  // For each referred, get forum content to get the file hash data
  var returnList = <dynamic>[];
  for (var forumId in referredLinks.keys) {
    var x = await rs.RsGxsForum.getForumContent(forumId, referredLinks[forumId]);
    returnList.addAll(x);
  }
  var downloadingHashes = await getFileDownloads();
  returnList.forEach((element) {
    element['mMeta']['isDownloading'] = isPostDownloading(element, downloadingHashes);
  });
  return sortPostList(returnList.map((x) => x = RsMsgMetaData.fromJson(x['mMeta'])).toList());
}

/// Check if a post `getForumContent` in `Map<String, dynamic>` format has a file and is
/// downloading in a  [downloadingHashes] list.
///
/// Return true if the payload data exists inside the [downloadingHashes]
bool isPostDownloading(Map<String, dynamic> post, List downloadingHashes) {
  // Delete malformed if body is malformed. Can happen due to a bad referred content
  if (post['mMsg'] == '') return false;
  var postbody = models.PostBody.fromJson(post['mMsg']);
  // If payloadlinks are not empty mean that can be downloading content
  if (postbody.payloadLinks.isNotEmpty) {
    for (var payloadLink in postbody.payloadLinks) {
      // Here we check if the payload link is already downloaded or downloading
      // If downloading activated delete already downloaded
      // Else delete downloading
      if (downloadingHashes.contains(payloadLink.hash)) return true;
    }
  }
  // If payload link is empty mean that is content without file, so remove it
  // if you are looking downloading content
  return false;
}

/// Return a list of file downloads performing [fileClearCompleted] before
///
/// Used to check when a bookmark is being download.
Future<List> getFileDownloads() async {
  await rs.RsFiles.fileClearCompleted();
  return await rs.RsFiles.fileDownloads();
}


/// Check if a post exists on the user bookmarks
///
/// Used, for example, to know if a publication with a payload is already
/// bookmarked
Future<bool> isBookmarked(forumId, postId) async =>
  (await Caches.userBookmarksCache.referredPostIds).contains('$forumId $postId');

/// Check if a post is bookmarked.
///
/// May do the logic on an empty cache if not updated before
bool isBookmarkedSync(forumId, postId) =>
   Caches.userBookmarksCache.dataSync
      .map((e) => e.postMetadata.referred) // Map referred ids
      .cast<String>()
      .toList()
      .contains('$forumId $postId');

