/*
 *  elRepo.io decentralized culture repository
 *
 *  Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */

part of '../repo.dart';


// ********** CIRCLEs ********** //

/// Create a circle and add your default identity to it
///
/// Where [identity] is your own identity to add to the circle. You can also
/// define a list of [invitedIds] to send invites to the circle.
/// Return circleId
///
/// If add [addTiers] it will add identities of tiers to help to propagate. Is
/// [true] by default
Future<String> createCircle(String circleName,
    {String identity, List<String> invitedIds, bool addTiers = true}) async {
  print('Creating circle $circleName');
  var circleId = await rs.RsGxsCircles.createCircle(circleName, RsGxsCircleType.PUBLIC);
  identity ??= rs.authIdentityId;
  // First add yourself to the circle
  if (identity != null) {
    await rs.RsGxsCircles.requestCircleMembership(circleId, identity);
    invitedIds.add(identity); // After request we need to invite also
  }
  // Add tiers for propagation
  if(addTiers) {
    invitedIds.addAll((await getTiersIdentities()).map((e) => e.mId));
  }
  // Invite people
  if (invitedIds != null) {
    await rs.RsGxsCircles.inviteIdsToCircle(invitedIds, circleId);
  }
  return circleId;
}

/// Edit a circle
///
///
Future<Map<String, dynamic>> editCircle(
    String circleId, String circleNewName, List<String> invitedPeople) async {
  var circleList = await rs.RsGxsCircles.getCirclesInfo([circleId]);
  circleList[0]['mMeta']['mGroupName'] = circleNewName;
  circleList[0]['mInvitedMembers'] = invitedPeople;
  return await rs.RsGxsCircles.editCircle(circleList[0]);
}

/// Return a list of circlesSummaries + circleDetails
///
/// First call circle summaries and then append on the response a `["details"]`
/// object with the call to getCircleDetails.
///
/// May be called twice because Retroshare backend needs to cache circle details
///
/// This will update the [Caches.circlesCache]
Future<List<dynamic>> getCirclesDetails() async {
  var circleList = await Caches.circlesCache.getCirclesSummaries();
  // Get details from the circle
  for (var circle in circleList) {
    try {
      circle['details'] =
      await rs.RsGxsCircles.getCircleDetails(circle['mGroupId']);
    } catch (e) {
      print(e);
    }
  }
  return circleList;
}

/// Return a list of subscribed circles
///
/// This circles have the `mSubscribeFlag` set to `7`
Future<List<dynamic>> getSubscribedCirclesList() async =>
    (await Caches.circlesCache.subscribedCircles).entries
        .map((value) => value.value).toList();

/// Get specific subscribed circle summary from de cache.
///
/// If cache is empty it await to sync fresh data from circleSummaries
Future<Map<String,dynamic>> getSubscribedCircle(String id) async =>
    (await Caches.circlesCache.subscribedCircles)[id] ?? <String, dynamic>{};

/// Get specific subscribed circle summary from de cache.
///
/// If no cached circle it return empty map and start caching asynchronous.
Map<String,dynamic> getSubscribedCircleCached(String id) =>
    Caches.circlesCache.subscribedCirclesCached[id] ?? <String, dynamic>{};

/// Get sorted list of circles.
///
/// It get a list of circle details and sort it by `mSubscriptionFlags`. In this
/// version **the circles you not belong are deleted**.
///
/// If you join/revoke an invite of a circle, the backend will not show the
/// changes until 20 or 30 seconds.
Future<List<dynamic>> getSortedCircles() async {
  var circleList = await getCirclesDetails();
  // Delete circles you don't own, you don't belong or you are not invited
  circleList.removeWhere((circle) {
    var mSubscriptionFlag = <String, dynamic>{};
    // If details can't be retrieved
    if (!circle.containsKey('details')) return true;
    // Check if your identity is present on the circle
    for (var id in circle['details']['mSubscriptionFlags']) {
      if (id['key'] == rs.authIdentityId) mSubscriptionFlag = id;
    }
    if (mSubscriptionFlag.isEmpty) {
      return true;
    } else if (mSubscriptionFlag['value'] &
    RsGxsCircleSubscriptionFlags.IN_ADMIN_LIST !=
        RsGxsCircleSubscriptionFlags.IN_ADMIN_LIST) return true;
    // Store your identity flag in a variable to don't search it later
    circle['details']['myFlag'] = mSubscriptionFlag;
    return false;
  });
  // Here our circList have only circles you own, you belong and you are invited.
  for (var circle in circleList) {
    // Put owned circles at the beggining
    if (circle['details']['mAmIAdmin']) {
      circleList.remove(circle);
      circleList.insert(0, circle);
    }
  }
  return circleList;
}

/// Check if a flag represent that is invited to a circle
///
/// Using bit wise operators it check if is invited but invite is not accepted.
/// You will find [myFlag] in `circle["details"]['myFlag']['value']`
bool imInvitedToACircle(int myFlag) =>
    myFlag != 7 &&
        // Probably this bit wise operation could be replaced by ['details']['mAmIAllowed']
        myFlag & RsGxsCircleSubscriptionFlags.IN_ADMIN_LIST ==
            RsGxsCircleSubscriptionFlags.IN_ADMIN_LIST;

/// For a circle id return true if you belong to a circle or circle id is empty
/// or `"00000000000000000000000000000000"` (which mean that is not a circle id)
bool iBelongToACircle(String circleId) =>
    circleId == null || circleId.isEmpty ||
        circleId == '00000000000000000000000000000000' ||
        Caches.circlesCache.subscribedCirclesCached.containsKey(circleId);

