/*
 *  elRepo.io decentralized culture repository
 *
 *  Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */

part of '../repo.dart';

/// Get cached identities
Map<String, Identity> cachedIdentitiesMap() =>
    Caches.identitiesCache.rsGxsToIdentityMap;

/// Get single cached identity by [rsGxsId]
Identity getCachedIdentity(String rsGxsId) =>
    Caches.identitiesCache.rsGxsToIdentityMap[rsGxsId];

/// Wrapper of [rs.RsIdentity.getIdentitiesSummaries] with simple Cache
/// implementation.
///
/// It return a fresh [getIdentitiesSummaries] result but update the cache.
Future<List<Identity>> getIdentitiesSummaries() async =>
    await Caches.identitiesCache.updateSummaries();

/// Return a list of followed authors info
///
/// It get all identities and return the getIdentitiesInfo frome those that have
/// the flag "mIsAContact". It return the identities ordered by alphabetically.
Future<List<dynamic>> getFollowedAuthorsInfo() async {
  var ids = await getIdentitiesSummaries();
  var idsInfo = await rs.RsIdentity.getIdentitiesInfo(
      <String>[for (var id in ids) id.mId])
    ..removeWhere((id) => !id.isContact)
    ..sort(
            (a, b) => a.name.compareTo(b.name));
  return idsInfo;
}

/// For a given [identityId] check if `mIsAContact` is set to true
///
/// todo: implement on the wrapper the method `isARegularContact` to check this
Future<bool> identityIsContact (String id) async =>
  (await rs.RsIdentity.getIdentitiesInfo([id]))[0].isContact;


/// Get your default indentity details
///
/// It get info from [rs.authIdentityId]. Cached or not.
Future<Identity> getOwnIdentityDetails() async =>
    await Caches.identitiesCache.ownIdentity;

/// Update own identity with new details.
///
/// Return new updated identity or null in case of error
Future<Identity> updateOwnIdentity(String id, String name, RsGxsImage img) async =>
    (await rs.RsIdentity.updateIdentity(id, name, img))
        ? Caches.identitiesCache.updateOwnIdentity()
        : null;

/// Get all user forums and subscribe to it
///
/// Return updated after subcription user forum list. Also it set the user as
/// contact on RetroShare backend.
///
/// Use the [follow] parameter to follow or unfollow the identity
Future<List<dynamic>> followUser(String userId, bool follow) async {
  await rs.RsIdentity.setAsRegularContact(userId, follow);
  var userForums = await getUserForum(userId);
  await subscribeToForums(userForums, follow);
  return userForums;
}

/// Get single identity details
///
/// This is not using `getIdDetails` endpoint, instead is using
/// `getIdentitiesInfo`. This is because we want just one type of identity info
/// metadata, and each method return different types of metadata.
/// todo: implement cache of this preventing how to manage an avatar update
Future<Identity> getIdDetails(String identityId) async {
  final identityDetails = await rs.RsIdentity.getIdentitiesInfo([identityId]);
  var details = identityDetails.isNotEmpty ? identityDetails[0] : null;
  if (details == null) {
    // If id details is null mean that [_getIdDetails] function
    // didn't return info about the Id.
    // This probably mean that we didn't synced the info on RS
    // yet. So we request to the network this info.
    Future.delayed(const Duration(seconds: 1), () async {
      await rs.RsIdentity.requestIdentity(identityId);
      if(_idDetailsRetries >= 5) getIdDetails(identityId);
    });
  }
  return details;
}
/// This variable is used to give maximum retries to [getIdDetails] because
/// it could call recursively asking to retroshare the
/// [rs.RsIdentity.requestIdentity]
var _idDetailsRetries = 0;

/// Get identities signed by a specific node [pgpId]
///
/// On [getIdentitiesSummaries] result there are a `mServiceString` containing
/// the `pgpId` of a signed node.
///
/// If cache is already populated it take it from the identities cache
Future<List<Identity>> getPGPSignedIdentities(String pgpId) async {
  var idsList = Caches.identitiesCache.rsGxsToIdentityMap.values.toList();
  if(idsList.isEmpty) {
    idsList = (await getIdentitiesSummaries());
  }
  return idsList..removeWhere(
          (id) => !isIdentityFromPgpId(pgpId, id)
  );
}

/// Check if identity is signed by a PGP id.
///
/// Return [true] if it is. It check on [id] the
/// [Identity.rawIdentitySummary['mServiceString']] to check if it appear the
/// [pgpId]
bool isIdentityFromPgpId(String pgpId, Identity id) =>
    id.rawIdentitySummary['mServiceString']  != null
        ? (id.rawIdentitySummary['mServiceString'] as String).contains(pgpId)
        : id.rawIdentityInfo['mPgpId'].contains(pgpId);

/// Same as [isIdentityFromPgpId] but cehcking2 multiple [pgpId]
///
/// Not used
bool isIdentityFromPgpIdList(List<String> pgpIds, Identity id) {
  for(var pgpId in pgpIds) {
    if(isIdentityFromPgpId(pgpId, id)) return true;
  }
  return false;
}

/// Used to retrieve tier1 linked identities
Future<List<Identity>> getTiersIdentities() async =>
    await getPGPSignedIdentities(TIER1_PGP_ID);

/// For an identity check if is owned by tier1 pgp id
bool isIdOwnedByTier(Identity id) =>
  isIdentityFromPgpId(TIER1_PGP_ID, id);

