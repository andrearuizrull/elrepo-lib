/*
 *  elRepo.io decentralized culture repository
 *
 *  Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */
part of '../repo.dart';

/// Related functions to retrieve forums
///
/// Except for bookmark forums and search


/// Function that return all tags forums summaries with synced content
///
/// When the post summary is retrieved, it remove occurrences with
/// `forum["mLastPost"]["xstr64"] == "0"`, preventing to return tag forums with
/// anything synchronized.
Future<List> getTagNames() async {
  print('Getting tag names');
  var tagForums = (await getForumsByName('${cnst.FORUM_PREPEND}${cnst.API_VERSION}_TAG_'))
  // This removewhere is not working well because sometimes the values are not 0 but the content is not sync yet
  // So it return a forum without posts
    ..removeWhere((forum) =>
    forum['mLastPost']['xstr64'] == '0' ||
        forum['mVisibleMsgCount'] == 0);

  var allTags = <dynamic>{};
  for (Map forum in tagForums) {
    // TODO(): check to only subscribe when needed.
    if ((forum['mSubscribeFlags']|rs_models.GROUP_SUBSCRIBE_SUBSCRIBED) != 0) {
      subscribeToForum(forum['mGroupId'], true);
    }
    allTags.add(forum['mGroupName']
        .substring(forum['mGroupName'].lastIndexOf('_TAG_') + '_TAG_'.length));
  }
  print('Found tags: $allTags');
  return allTags.toList();
}

void syncForums() {
  rs.RsGxsForum.requestSynchronization();
}

/// Get the USER forum metadata for the user with [identityId].
Future<List<dynamic>> getUserForum(String identityId) async {
  final userForumName = getForumName('USER', identityId);
  final requestedForums = await getForumsByName(userForumName);
  return requestedForums;
}

/// Get the USER [identityId] forums RsMsgMetaData sorted by timestamp
Future<List<RsMsgMetaData>> getUserForumSortedMsgs(String identityId) async =>
   sortRsMsgMetaDataByTimestamp(
       await getForumsMsgMetadata(
           await getUserForum(identityId)
       )
   );


/// From the USER forum, get post headers.
///
/// Basically it return post headers publuished by [rs.authIdentityId] accessing
/// the USER forum.
///
/// Ordered by Timestamp
Future<List<RsMsgMetaData>> getOwnPublishedPostHeaders() async =>
  sortRsMsgMetaDataByTimestamp(
      await getPostHeaders('USER', rs.authIdentityId));


/// From a list of forums, it return a list of all posts metadata
///
/// For example if you want to retrieve multiple CONTENT forums, you will get
/// back all posts metadata you can see in this forums.
Future<List<RsMsgMetaData>> getForumsMsgMetadata(List<dynamic> forums) async {
  return [
    for (var forum in forums)
      await rs.RsGxsForum.getForumMsgMetaData(forum['mGroupId'])
  ].expand((i) => i).toList();
}




/// Return posts headers for a specific elrepoio forum.
///
/// Where [forumType] is the tag of the forum you want to retrieve, `CONTENT` by
/// default but can be `TAG`, `USER`... Check [forumTypes]
///
/// Where [label] is the appended label for the forum, for example if you want
/// to retrieve post headers from
/// `elRepo.io_0.0.14_USER_230fdb9c4f1049cd44100a09402122f7`, [forumType] will
/// be `USER` and [label] the identity of the user you want to retrieve, in this
/// case `230fdb9c4f1049cd44100a09402122f7`
///
/// For TAG's forum the name is like: `elRepo.io_0.0.14_TAG_FreeSoftware`
Future<List<RsMsgMetaData>> getPostHeaders(
    [String forumType = 'CONTENT', String label = '']) async {
  // Asyncronously request syncronization
  syncForums();
  final requestedForumName = getForumName(forumType, label);
  print('Retrieving post headers from ' + requestedForumName);
  var allPosts = <RsMsgMetaData>[];
  final requestedForums = await getForumsByName(requestedForumName);
  for (Map forum in requestedForums) {
    print('Found forum: ${forum["mGroupName"]} / id: ${forum["mGroupId"]}');
// TODO(nicoechaniz): correctly filter depending on forum["mSubscribeFlags"] values before trying to subscribe
    subscribeToForum(forum['mGroupId'], true);
    allPosts.addAll(await _getPostsMetadata(forum));
  }
  return allPosts;
}

/// Get Forum Content
///
/// From a forum with specific [forumId] and [postId] get the
/// `/rsGxsForums/getForumContent` information. Like the forum inner message.
///
/// Return the post details Map
Future<Map<String, dynamic>> getPostDetails(String forumId, String postId,
    [bool isLinkPost]) async {
  final details = await rs.RsGxsForum.getForumContent(forumId, [postId]);
  if (isLinkPost) {
    var postMetadata = models.PostMetadata.fromJsonString(details[0]['mMeta']['mMsgName']);
    try {
      final realPostIds = postMetadata.referred.split(' ');
      final realPostDetails = await rs.RsGxsForum.getForumContent(
          realPostIds[0], [realPostIds[1]]
      );
      return realPostDetails[0];
    } catch (e) {
      print('Could not retrieve original post $e');
      return {};
    }
  }
  return details[0];
}

/// From a list of post headers, get all referred links and return its in a map
/// forumId : [List of requested posts]
Map<String, List<String>> getReferredsFromPostHeader(
    List<RsMsgMetaData> postHeaders) {
// To optimize calls to the API, lets group all requested posts in a map where:
// forumId : [List of requested posts]
  var forumsToPosts = <String, List<String>>{};
  postHeaders.forEach((postMeta) {
    try {
      List referred = postMeta.postMetadata.referred.split(' ');
      if (!forumsToPosts.containsKey(referred[0])) {
        forumsToPosts[referred[0]] = [];
      }
      forumsToPosts[referred[0]].add(referred[1].toString());
    } catch (e) {
      print(e);
    }
  });
  return forumsToPosts;
}


/// This function will return a digested CONTENT forums posts headers metadata.
///
/// It mean that this function sort the content to show by certain priorities,
/// for example, it show following users first.
Future<List<RsMsgMetaData>> exploreContent() async {
  // var bookmarkedIds = await Caches.userBookmarksCache.referredPostIds;
  // contentsList.removeWhere( (element) =>
  // Delete own posts and bookmarked posts
  // element.mAuthorId == rs.authIdentityId ||
  //     bookmarkedIds.contains(element.mMsgId)
  // );
  return await sortPostList(await getPostHeaders());
}

/// Return the forum name in format [${cnst.FORUM_PREPEND}${cnst.API_VERSION}_$forumType$label]
String getForumName(String forumType, [String label = '']) {
  if (!forumTypes.contains(forumType)) {
    throw Exception('invalid forum type');
  }
  if (label != '') {
    label = '_$label';
  }
  return "${cnst.FORUM_PREPEND}${cnst.API_VERSION}_$forumType$label";
}

/// Get forums metadata for forums that starts with name [forumName]
///
/// You have to pass [forumName] in format
/// `${cnst.FORUM_PREPEND}${cnst.API_VERSION}_$forumType$label` for example
/// calling [getForumName()] function.
Future<List> getForumsByName(String forumName) async {
  final allForums = await rs.RsGxsForum.getForumsSummaries();
  // final allForumNames = allForums.map((i) => i['mGroupName']).toList();
  // print('All known forum names: $allForumNames');
  final requestedForums = allForums.where(
          (i) => i['mGroupName'].startsWith(forumName)
  ).toList();
  return requestedForums;
}

String _getUserForumName() =>
    '${cnst.FORUM_PREPEND}${cnst.API_VERSION}_USER_${rs.authIdentityId}';

String _getUserBookmarkName() =>
    '${cnst.FORUM_PREPEND}${cnst.API_VERSION}_BOOKMARKS_${rs.authIdentityId}';

/// Get comments from a forum/post id
///
/// Sorted by the older first
Future<List<Map<String, dynamic>>> getForumComments(String forumId, String postId) async {
  String publishTs(Map post) => post['mMeta']['mPublishTs']['xstr64'];
  var comments = await rs.RsGxsForum.getChildPosts(forumId, postId);
  comments.sort((a, b) => publishTs(a).compareTo(publishTs(b)));
  // comments.sort((a, b) => publishTs(b).compareTo(publishTs(a))); // Newer first
  return comments;
}

// todo(kon): not used
// Future<List> getForumMetadata(String forumId) {
//   return rs.RsGxsForum.getForumMsgMetaData(forumId);
// }
