/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

part of models;

// options
class ContentTypes {
  static const String ref = 'ref';
  static const String image = 'image';
  static const String audio = 'audio';
  static const String video = 'video';
  static const String document = 'document';
  static const String text = 'text';
  static const String file = 'file';
}

class MarkupTypes {
  static const String plain = 'plain';
  static const String markdown = 'markdown';
  static const String html = 'html';
}

List forumTypes = ['CONTENT', 'TAG', 'USER', 'BOOKMARKS'];

class PostRoles {
  static const String post = 'post';
  static const String comment = 'comment';
  static const String firstPublication = 'firstPublication';
}

// models
class PayloadLink {
  String filename = '';
  String hash = '';
  String link = '';
  // TODO(nicoechaniz): hash can be extracted from the link. Implement that where needed and remove hash from here
  Map<String, dynamic> toJson() =>
      {'filename': filename, 'hash': hash, 'link': link};

  PayloadLink(this.filename, this.hash, this.link);
}

/// The model of the JSON object stored in the RS forum post body
///
/// This describe our JSON object stored inside the post body, this **isn't the
/// model** for the RS RsGxsForumMsg object
class PostBody {
  String text = '';
  List<PayloadLink> payloadLinks = [];
  List<String> tags = [];

  PostBody();

  Map<String, dynamic> toJson() =>
      {'text': text, 'payloadLinks': payloadLinks, 'tags': tags};

  PostBody.fromJson(String rawBody) {
    Map jsonBody = jsonDecode(rawBody);
    text = jsonBody['text'];
    if (jsonBody['payloadLinks'].length > 0) {
      for (Map linkData in jsonBody['payloadLinks']) {
        payloadLinks.add(PayloadLink(
            linkData['filename'], linkData['hash'], linkData['link']));
      }
    }
    if (jsonBody['tags'] != null && jsonBody['tags'].length > 0) {
      tags = jsonBody['tags'].cast<String>();
    }
  }
}

/// This model describe the metadata stored on a RS Forum Post title
///
/// You get the title when you ask for getForumMsgMetaData, and there we store
/// this information useful for elrepo.io
///
/// This **is not** the model for the RS RsMsgMetaData object
class PostMetadata {
  String title;
  String summary;
  String markup;
  String contentType;
  String role;
  String referred;
  String circle;

  PostMetadata(
      {this.title,
      this.summary,
      this.markup,
      this.contentType,
      this.referred,
      this.circle,
      this.role});

  // get metadata for a linkPost
  PostMetadata.generateLinkPostMetadata(
      PostMetadata postMetadata, String msgId, String forumId)
      : this(
            title: postMetadata.title,
            summary: postMetadata.summary,
            markup: postMetadata.markup,
            contentType: postMetadata.contentType,
            role: postMetadata.role,
            circle: postMetadata.circle,
            referred:
                '$forumId $msgId'); // reference to the actual content post

  Map<String, dynamic> toJson() => {
        'title': title,
        'summary': summary,
        'markup': markup,
        'contentType': contentType,
        'role': role,
        'referred': referred,
        'circle': circle
      };

  /// From a JSON string collected from the forum `mMsgName` it instantiate a
  /// PostMetadata object
  ///
  /// The string is decoded inside this function using [jsonDecode].
  ///
  /// If some error during the decodification/instantiation, it return a dummy
  /// PostMetadata object with 'bad parsing' string as title and summary.
  factory PostMetadata.fromJsonString(String jsonString) {
    try {
      final json = jsonDecode(jsonString);
      return PostMetadata(
          title: json['title'],
          summary: json['summary'],
          markup: json['markup'],
          contentType: json['contentType'],
          role: json['role'],
          circle: json['circle'],
          referred: json['referred']);
    } catch (e) {
      print("Can't decode " + jsonString);
      return PostMetadata(
          title: 'bad parsing',
          summary: 'bad parsing',
          markup: MarkupTypes.plain,
          contentType: ContentTypes.text,
          role: PostRoles.post,
          referred: '');
    }
  }

  String get referredForumId => referred?.split(' ')[0];
  String get referredPostId => referred?.split(' ')[1];
}

class PostData {
  PostMetadata metadata = PostMetadata(
      markup: MarkupTypes.plain,
      contentType: ContentTypes.text,
      role: PostRoles.post);
  PostBody mBody = PostBody();
  String filePath = '';
  String filename = '';
  String project =
      ''; // Used for import datasets. The project name of the original data, ex: gutemberg
  String originUrl = ''; // Used for import datasets. Url for the original data.
}

class PublishData {
  String msgId;
  String forumId;
  List hashTags;

  PublishData(this.forumId, this.msgId, this.hashTags);
}

// ********** elrepo.io gateway related functions ********** //

class GatewayAuthObject {
  String user, password, identity;
  GatewayAuthObject(this.user, this.password, this.identity);

  GatewayAuthObject.fromJson(Map<String, dynamic> json)
      : user = json['user'],
        password = json['password'],
        identity = json['identity'];

  Map<String, dynamic> toJson() => {
        'user': user,
        'password': password,
        'identity': identity,
      };
}

/// Events passed to a callback during a publishing action
enum PublishEvents {
  /// Throw if starting to hash a file
  HASHING_FILE,

  /// When publishing into a CONTENT forum
  PUBLISHING_CONTENT,

  /// When creating TAG forums
  CREATING_TAGS,

  /// When linking it to USER forum
  BOOKMARKING,
}
